import requests
import json
import os
import base64


url = 'http://vm-shushi:8888/model_create'
file_name = 'C:\Users\shushi\Documents\TOSCA\Models\DCAE\\new_controller_example\json_spec\cuda.json'
fileh = open(file_name, 'r')
filecontent = json.load(fileh)

ret = {}
ret['spec'] = filecontent

result = requests.post(url, json.dumps(ret))
data = json.loads(result.text)
if data.has_key('schema'):
    print base64.decodestring(data['schema'])
if data.has_key('template'):
    print base64.decodestring(data['template'])
if data.has_key('translate'):
    print base64.decodestring(data['translate'])

